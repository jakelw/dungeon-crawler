﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace _3dGame
{
   public class Chest :Thing
    {
        bool opened = false;
        float lidLocation = 0.0f;
        public Vector2 GridPosition = Vector2.Zero;
 


        public override void update()
        {
            if (opened == true)
                lidLocation += 0.015f;
            WorldMatrix = Matrix.CreateRotationY(MathHelper.ToRadians(90) * facing) * Matrix.CreateTranslation(Position);
        }
        public override void Spawn(Vector3 startposition, string Name, ContentManager theContentManager)
        {
            WorldMatrix = Matrix.CreateTranslation(Vector3.Zero);
            Position = startposition;
            model = theContentManager.Load<Model>(Name);
            GridPosition = new Vector2(startposition.X /4,startposition.Z/4);

            WorldMatrix = Matrix.CreateRotationY(MathHelper.ToRadians(90) * facing) * Matrix.CreateTranslation(Position);
            Game1.chests.Add(this);
            Game1.things.Add(this);

        }
        public override void DrawModel(Matrix view, Matrix projection)
        {

            Matrix[] meshWorldMatrices = new Matrix[2];
            meshWorldMatrices[0] = Matrix.CreateTranslation(new Vector3(0,lidLocation, 0));
            meshWorldMatrices[1] = Matrix.CreateTranslation(new Vector3(0, 0, 0));

            WorldMatrix = Matrix.CreateRotationY(MathHelper.ToRadians(90) * facing) * Matrix.CreateTranslation(Position);

            for (int index = 0; index < model.Meshes.Count; index++)
            {
                ModelMesh mesh = model.Meshes[index];
                foreach (BasicEffect effect in mesh.Effects)
                {

                    
                    effect.FogEnabled = true;
                    effect.FogColor = Game1.bgColor.ToVector3(); // For best results, ake this color whatever your background is.
                    effect.FogStart = Game1.fog.X;
                    effect.FogEnd = Game1.fog.Y;


                    effect.World = mesh.ParentBone.Transform * meshWorldMatrices[index] * WorldMatrix;
                    effect.View = view;
                    effect.Projection = projection;
                }

                mesh.Draw();
            }
        }
        public void Animate()
        {
            opened = true;
        }
    }
}
