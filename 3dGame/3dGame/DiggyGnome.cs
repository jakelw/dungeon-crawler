﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace _3dGame
{
    public class DiggyGnome
    {
        public Vector2 location;
        public int stepsLeft = 1000;

        public void Begin(ContentManager theContentManager)
        {
            while (stepsLeft > 0)
            {
                int randomness = Game1.rand.Next(1, 3);
                if (Game1.Rooms.ContainsKey(location) && randomness == 1)
                    Game1.Rooms[location].Change("emptyarea", theContentManager, true);
                if (Game1.Rooms.ContainsKey(location) && randomness == 2)
                    Game1.Rooms[location].Change("emptyarea2", theContentManager, true);

                int monsterspawner = Game1.rand.Next(1, 101);
                //monsters
                if (monsterspawner == 7&& Game1.Enemies.ContainsKey(location) == false)
                {
                    Enemy newguy = new Enemy();
                    newguy.Spawn("orcGamePiece", location*4, theContentManager,.75f);
                }
                if (monsterspawner == 8 && Game1.Enemies.ContainsKey(location) == false)
                {
                    Enemy newguy = new Enemy();
                    newguy.Spawn("GiantOrc", location * 4, theContentManager,.33f);
                } 
                if (monsterspawner == 9 && Game1.Enemies.ContainsKey(location) == false)
                {
                    Chest newguy = new Chest();
                    newguy.Spawn(new Vector3(location.X*4,0,location.Y*4), "chesttest",theContentManager);
                }
                

                //items
                


                Vector2 direction = new Vector2(1,1);
                while (direction.X !=0 && direction.Y!=0)
                direction = new Vector2(Game1.rand.Next(-1, 2), Game1.rand.Next(-1, 2));
                location += direction;
                stepsLeft--;

            }
            foreach (var room in Game1.Rooms)
                if (room.Value.gridlocation.X < -24 || room.Value.gridlocation.X > 24 || room.Value.gridlocation.Y <-24 || room.Value.gridlocation.Y > 24)
                    Game1.Rooms[room.Key].Change("filledSquare2", theContentManager, false);

        }
        public void Reset()
        {
            location = new Vector2(0, 0);
            stepsLeft = 1000;
        }
    }
}
