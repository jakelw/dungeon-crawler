﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace _3dGame
{
   public  class Thing
    {
       public Matrix WorldMatrix;
    public Vector3 Position = Vector3.Zero;
    public int facing = 1;
    
    public Model model;
        

        public virtual void Spawn(Vector3 startposition, string Name, ContentManager theContentManager)
        {
            WorldMatrix = Matrix.CreateTranslation(Vector3.Zero);
            Position = startposition;
             model = theContentManager.Load<Model>(Name);

            
        WorldMatrix = Matrix.CreateRotationY(MathHelper.ToRadians(90)*facing) *Matrix.CreateTranslation(Position);
        Game1.things.Add(this);
            
        }
        public virtual void update()
        {
            WorldMatrix = Matrix.CreateRotationY(MathHelper.ToRadians(90) * facing) * Matrix.CreateTranslation(Position);
        }
        public virtual void DrawModel(Matrix view, Matrix projection)
        {
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    
                    
                    effect.FogEnabled = true;
                    effect.FogColor = Game1.bgColor.ToVector3(); // For best results, ake this color whatever your background is.
                    effect.FogStart = Game1.fog.X;
                    effect.FogEnd = Game1.fog.Y;
                     

                    effect.World = WorldMatrix;
                    effect.View = view;
                    effect.Projection = projection;
                }

                mesh.Draw();
            }
        }

    }
}
