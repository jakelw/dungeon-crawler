﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _3dGame
{
    public class GridSpace : Thing
    {
        public bool occupied = false;
        string typeOfSpace;
        public bool passable = false;
        public Vector2 gridlocation = Vector2.Zero;
        public void Spawn( string assetName,ContentManager theContentManager, Vector2 position)
        {
            typeOfSpace = assetName;

            gridlocation = position;
            base.Spawn(new Vector3(gridlocation.X * 4,0,gridlocation.Y*4), assetName, theContentManager);
        }
        public void Change(string assetname,ContentManager theContentManager, bool passability)
        {
            passable = passability;
            base.model = theContentManager.Load<Model>(assetname);
        }
        public void Change(bool passability)
        {
            passable = passability;
            
        }
    }
}
