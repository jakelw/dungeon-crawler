﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _3dGame
{
    class Deck
    {
        public List<Card> Cards = new List<Card>();
        public Card DrawCard()
        {
            int i = Game1.rand.Next(0, Cards.Count -1);

            return Cards[i];

        }
    }
}
