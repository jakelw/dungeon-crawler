﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _3dGame
{
    public class Camera
    {
        public Vector3 Position; // where the camera itself is in space
        public float cameraAngle;
       public  Vector3 cameraTarget = Vector3.Zero; // where is the camera looking
       float yview = 0;
        public Matrix viewMatrix;
        public void update(Vector3 newpos, float angle, float yView)
        {
            
            yview += yView;
            Position = newpos;
            cameraAngle = angle;
            cameraTarget.X = (float)(Position.X + 10 * Math.Cos(cameraAngle));
            cameraTarget.Z = (float)(Position.Z + 10 * Math.Sin(cameraAngle));
            cameraTarget.Y = yview + Game1.PlayerEntity.Position.Y ;
            viewMatrix = Matrix.CreateLookAt(Position, cameraTarget, Vector3.Up);

            
        }
    }
}
