﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace _3dGame
{
    public class Sprite
    {
        
        public string AssetName;
        bool visible= true;
        public Rectangle Size;
        private float mScale = 1.0f;
        public float angle;
        public float Scale
        {

            get { return mScale; }

            set
            {

                mScale = value;



                Size = new Rectangle(0, 0, (int)(backgroundImage.Width * Scale), (int)(backgroundImage.Height * Scale));

            }

        }

        public Vector2 Position = new Vector2(0, 0);


        private Texture2D backgroundImage;
        public bool IsVisible()
        {
            return visible;
        }
        public void ChangeVisible(bool visibility)
        {
            visible = visibility;
        }

        public void LoadContent(ContentManager theContentManager, string theAssetName)
        {

            backgroundImage = theContentManager.Load<Texture2D>(theAssetName);

            AssetName = theAssetName;

            Size = new Rectangle(0, 0, (int)(backgroundImage.Width * Scale), (int)(backgroundImage.Height * Scale));

        }
        public void ChangeScale(float size)
        {
            Scale = size;
        }

        public virtual void Draw(SpriteBatch theSpriteBatch)
        {
            Vector2 origin = new Vector2(backgroundImage.Width / 2, backgroundImage.Height / 2);
            theSpriteBatch.Draw(backgroundImage, Position,

                new Rectangle(0, 0, backgroundImage.Width, backgroundImage.Height),

                Color.White, angle, origin, Scale, SpriteEffects.None, 0);




        }
        public void Update(GameTime theGameTime, Vector2 theSpeed, Vector2 theDirection)
        {

            Position += theDirection * theSpeed * (float)theGameTime.ElapsedGameTime.TotalSeconds;

        }

    }
}
