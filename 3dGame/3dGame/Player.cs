﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _3dGame
{
    public class Player
    {
        const int REALMOVESPEED = Game1.TRANSITIONSPEED;
        public Vector3 DirectionToGo = Vector3.Zero;
        public Vector3 Position;
        public Camera PlayerCamera = new Camera();
        public float Height=2f;
        bool canmove = true;
        float yview = 0;
        bool moving = false;
        public float speed = 1;
        public int total = REALMOVESPEED;
        int divider = REALMOVESPEED;
        public Vector2 gridspot = Vector2.Zero;
        public bool donewithturn = true;
        public float rotationAxisY = 0;
        Vector3 destination = Vector3.Zero;

        //weapons
        public Item Primary = new Item();
        public Item Secondary = new Item();
        //armors
        public Item Head = new Item();
        public Item Chest = new Item();
        public Item Gloves = new Item();
        public Item Pants = new Item();
        public Item Boots = new Item();
        //Spell
        public Item spell = new Item();




        bool canattack = true;


        public void spawn(Vector3 startPos)
        {
            Position = startPos;
            
        }
        public void see(Matrix ProjectionMatrix)
        {
            
            foreach (Thing thing in Game1.things)
                if (Vector3.Distance(Position,thing.Position) <25)
                thing.DrawModel(PlayerCamera.viewMatrix, ProjectionMatrix);
        }
        public void update()
        {
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);
            

            if (canmove == true && donewithturn == true)
            experimentalMove(gamePadState);
            //move(gamePadState);

            if (donewithturn == false)
                smoothMoves(DirectionToGo);
            KeyboardState state = Keyboard.GetState();

            if (gamePadState.DPad.Up == ButtonState.Released && gamePadState.DPad.Down ==
                ButtonState.Released && gamePadState.DPad.Left == ButtonState.Released && gamePadState.DPad.Right
                == ButtonState.Released && state.IsKeyDown(Keys.W) != true && state.IsKeyDown(Keys.A) != true
                && state.IsKeyDown(Keys.S) != true && state.IsKeyDown(Keys.D) != true)

                canmove = true;

            foreach (var enemy in Game1.Enemies)
                if (enemy.Value.donewithturn == false || donewithturn == false)
                {
                    canmove = false;
                    break;
                }

            if (gamePadState.Buttons.Y == ButtonState.Pressed)
            {
                Game1.clipping = false;
                Game1.fog = new Vector2(100, 101);
                foreach (var enemy in Game1.Enemies)
                {
                    enemy.Value.total = (int)(Game1.TRANSITIONSPEED / enemy.Value.speed) * (int)Game1.PlayerEntity.speed;
                    if (enemy.Value.total > 12)
                        enemy.Value.total = 12;
                }
            }
                // 
                Position.Y = Height;

                if (Game1.clipping == false)
                {
                    if (gamePadState.ThumbSticks.Left.X < 0)
                        Position += Vector3.Normalize(direction("Left") - Position) * -gamePadState.ThumbSticks.Left.X / 10;
                    if (gamePadState.ThumbSticks.Left.X > 0)
                        Position += Vector3.Normalize(direction("Right") - Position) * gamePadState.ThumbSticks.Left.X / 10;
                    Position += Vector3.Normalize(PlayerCamera.cameraTarget - Position) * gamePadState.ThumbSticks.Left.Y / 10;

                    speed = 10;
                    
                }
            if (Game1.ScreenToShow != "MainMenu")
                rotationAxisY += gamePadState.ThumbSticks.Right.X / 10;

                
            MouseState newState = Mouse.GetState();
            if (Game1.ScreenToShow != "MainMenu")
            {
                rotationAxisY += (float)(newState.X - 400) / 100;
                Mouse.SetPosition(400, 240);






                PlayerCamera.update(Position, rotationAxisY, gamePadState.ThumbSticks.Right.Y + -(float)(newState.Y - 240) / 10);
            }
        }
        Vector3 whichWay()
        {

            donewithturn = false;
            float distance = 4000;


            if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.North) < distance)
            {
                distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.North);
                destination = Game1.North;
                Game1.iamfacing = "North";
            }
            if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.East) < distance)
            {
                distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.East);
                destination = Game1.East;
                Game1.iamfacing = "East";
            }
            if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.South) < distance)
            {
                distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.South);
                destination = Game1.South;
                Game1.iamfacing = "South";
            }
            if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.West) < distance)
            {
                distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.West);
                destination = Game1.West;
                Game1.iamfacing = "West";
            }

            return destination;

        }
        Vector3 direction(string direction)
        {
            Vector3 result = Vector3.Zero;
            float pi = MathHelper.Pi;
            if (direction == "Left")
            {
                result.X = (float)(Position.X + 10 * Math.Cos(rotationAxisY + (1.5*pi)));
                result.Z = (float)(Position.Z + 10 * Math.Sin(rotationAxisY + (1.5 * pi)));
            }
            if (direction == "Right")
            {
                result.X = (float)(Position.X + 10 * Math.Cos(rotationAxisY + (.5 * pi)));
                result.Z = (float)(Position.Z + 10 * Math.Sin(rotationAxisY + (.5 * pi)));
            }
            result.Y = Position.Y;


            return result;
        }
        public void move(GamePadState gamePadState)
        {
            if (gamePadState.DPad.Up == ButtonState.Pressed && canmove == true)
            {
                float distance = 4000;


                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.North) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.North);
                    destination = Game1.North;
                }
                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.East) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.East);
                    destination = Game1.East;
                }
                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.South) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.South);
                    destination = Game1.South;
                }
                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.West) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.West);
                    destination = Game1.West;
                }
               // if (Game1.Rooms[new Vector2(destination.X, destination.Z)].passable == true)
                //{
                    Position += destination * 4;
                    // showmovement(destination * 4);
                    gridspot += new Vector2(destination.X, destination.Z);
               // }
                canmove = false;
                moving = true;
            }


            if (gamePadState.DPad.Down == ButtonState.Pressed && canmove == true)
            {
                float distance = 4000;


                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.North) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.North);
                    destination = Game1.North;
                }
                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.East) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.East);
                    destination = Game1.East;
                }
                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.South) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.South);
                    destination = Game1.South;
                }
                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.West) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.West);
                    destination = Game1.West;
                }
               // if (Game1.Rooms[new Vector2(-destination.X, -destination.Z)].passable == true)
                //{
                    Position += -destination * 4;
                    gridspot += new Vector2(-destination.X, -destination.Z);
               // }
                    canmove = false;
                    moving = true;
                
            }


            if (gamePadState.DPad.Right == ButtonState.Pressed && canmove == true)
            {
                float distance = 4000;


                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.North) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.North);
                    destination = Game1.North;
                }
                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.East) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.East);
                    destination = Game1.East;
                }
                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.South) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.South);
                    destination = Game1.South;
                }
                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.West) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.West);
                    destination = Game1.West;
                }
                Vector3 deststorer = destination;
               
                    float storer = destination.X;
                    destination.X = destination.Z;
                    destination.Z = storer;
                    if (deststorer != Game1.South)
                    {
                        Position += destination * 4;
                        gridspot += new Vector2(destination.X, destination.Z);
                    }
                    else
                    {
                        Position += -destination * 4;
                        gridspot += new Vector2(-destination.X, -destination.Z);
                    }
                    

                canmove = false;
                moving = true;
            }

            if (gamePadState.DPad.Left == ButtonState.Pressed && canmove == true)
            {
                float distance = 4000;


                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.North) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.North);
                    destination = Game1.North;
                }
                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.East) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.East);
                    destination = Game1.East;
                }
                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.South) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.South);
                    destination = Game1.South;
                }
                if (Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.West) < distance)
                {
                    distance = Vector3.Distance(PlayerCamera.cameraTarget, Position + Game1.West);
                    destination = Game1.West;
                }
                Vector3 deststorer = destination;
               
                    float storer = destination.X;
                    destination.X = destination.Z;
                    destination.Z = storer;

                    if (deststorer != Game1.South)
                    {
                        Position += -destination * 4;
                        gridspot += new Vector2(-destination.X, -destination.Z);
                    }
                    else
                    {
                        Position += destination * 4;
                        gridspot += new Vector2(destination.X, destination.Z);
                    }
                   
                canmove = false;
                moving = true;
            }


            if (gamePadState.DPad.Up == ButtonState.Released && gamePadState.DPad.Down ==
                ButtonState.Released && gamePadState.DPad.Left == ButtonState.Released && gamePadState.DPad.Right
                == ButtonState.Released)
                canmove = true;
        }
        public void showmovement(Vector3 whereto)
        {
            int total = 45;
            int divider = total;
            while (total > 0)
            {
                Position += whereto / divider;
                total--;
                
                PlayerCamera.update(Position, rotationAxisY, 0);
                Game1.PlayerEntity.see(Game1.ProjectionMatrix);
            }
        }
 
        public void experimentalMove(GamePadState gamePadState)
        {
            Vector2 direction = Vector2.Zero;
            //move forward
            KeyboardState state = Keyboard.GetState();
            if (canmove == true)
            {
                if ((state.IsKeyDown(Keys.W) || gamePadState.DPad.Up == ButtonState.Pressed )&& canmove == true && Game1.Rooms[new Vector2(whichWay().X, whichWay().Z) + gridspot].passable == true)
                {
                    //gridspot += new Vector2(whichWay().X, whichWay().Z);
                    direction = new Vector2(whichWay().X, whichWay().Z) *4;
                    //Position += whichWay() * 4;
                    //whereTheFuckAmIGoing = whichWay() * 4;
                    canmove = false;
                }
                // move backwards
                if ((state.IsKeyDown(Keys.S) || gamePadState.DPad.Down == ButtonState.Pressed) && canmove == true && Game1.Rooms[new Vector2(-whichWay().X, -whichWay().Z) + gridspot].passable == true)
                {

                   // Position -= whichWay() * 4;
                    //whereTheFuckAmIGoing = -whichWay() * 4;
                    //gridspot -= new Vector2(whichWay().X, whichWay().Z);
                    direction = new Vector2(-whichWay().X, -whichWay().Z) * 4;
                    canmove = false;
                }
                //move left
                if ((state.IsKeyDown(Keys.A) || gamePadState.DPad.Left == ButtonState.Pressed) && canmove == true)
                {
                    if (whichWay() == Game1.North && Game1.Rooms[new Vector2(Game1.West.X, Game1.West.Z) + gridspot].passable == true)
                    {
                        //Position += Game1.West * 4;
                       // whereTheFuckAmIGoing = Game1.West * 4;
                        //gridspot += new Vector2(Game1.West.X, Game1.West.Z);
                        direction = new Vector2(Game1.West.X, Game1.West.Z)*4;
                    }
                    if (whichWay() == Game1.East && Game1.Rooms[new Vector2(Game1.North.X, Game1.North.Z) + gridspot].passable == true)
                    {
                       // Position += Game1.North * 4;
                        //whereTheFuckAmIGoing = Game1.North * 4;
                        //gridspot += new Vector2(Game1.North.X, Game1.North.Z);
                        direction = new Vector2(Game1.North.X, Game1.North.Z)*4;
                    }
                    if (whichWay() == Game1.South && Game1.Rooms[new Vector2(Game1.East.X, Game1.East.Z) + gridspot].passable == true)
                    {
                        //Position += Game1.East * 4;
                        //whereTheFuckAmIGoing = Game1.East * 4;
                        //gridspot += new Vector2(Game1.East.X, Game1.East.Z);
                        direction = new Vector2(Game1.East.X, Game1.East.Z)*4;
                    }
                    if (whichWay() == Game1.West && Game1.Rooms[new Vector2(Game1.South.X, Game1.South.Z) + gridspot].passable == true)
                    {
                        //Position += Game1.South * 4;
                        //whereTheFuckAmIGoing = Game1.South * 4;
                       // gridspot += new Vector2(Game1.South.X, Game1.South.Z);
                        direction = new Vector2(Game1.South.X, Game1.South.Z)*4;
                    }



                    canmove = false;
                }
                //move right
                if ((state.IsKeyDown(Keys.D) || gamePadState.DPad.Right == ButtonState.Pressed) && canmove == true)
                {
                    if (whichWay() == Game1.North && Game1.Rooms[new Vector2(Game1.East.X, Game1.East.Z) + gridspot].passable == true)
                    {
                        //Position += Game1.East * 4;
                        //whereTheFuckAmIGoing = Game1.East * 4;
                        //gridspot += new Vector2(Game1.East.X, Game1.East.Z);
                        direction = new Vector2(Game1.East.X, Game1.East.Z)*4;
                    }
                    if (whichWay() == Game1.East && Game1.Rooms[new Vector2(Game1.South.X, Game1.South.Z) + gridspot].passable == true)
                    {
                        //Position += Game1.South * 4;
                        //whereTheFuckAmIGoing = Game1.South * 4;
                        //gridspot += new Vector2(Game1.South.X, Game1.South.Z);
                        direction = new Vector2(Game1.South.X, Game1.South.Z)*4;
                    }
                    if (whichWay() == Game1.South && Game1.Rooms[new Vector2(Game1.West.X, Game1.West.Z) + gridspot].passable == true)
                    {
                        //Position += Game1.West * 4;
                       // whereTheFuckAmIGoing = Game1.West* 4;
                       // gridspot += new Vector2(Game1.West.X, Game1.West.Z);
                        direction = new Vector2(Game1.West.X, Game1.West.Z)*4;
                    }
                    if (whichWay() == Game1.West && Game1.Rooms[new Vector2(Game1.North.X, Game1.North.Z) + gridspot].passable == true)
                    {
                        //Position += Game1.North * 4;
                        //whereTheFuckAmIGoing = Game1.North * 4;
                        //gridspot += new Vector2(Game1.North.X, Game1.North.Z);
                        direction = new Vector2(Game1.North.X, Game1.North.Z)*4;
                    }
                    canmove = false;
                }


                MouseState mouseState = Mouse.GetState();
                //attack button
                if ((gamePadState.Triggers.Right == 1 ||mouseState.LeftButton== ButtonState.Pressed ) && canmove == true && canattack == true)
                {
                    foreach (var enemy in Game1.Enemies)
                        if (enemy.Value.GridPosition == new Vector2(whichWay().X, whichWay().Z) + gridspot)
                            enemy.Value.Alive = false;
                    canattack = false;
                    canmove = false;
                }
                //open chests
                if ((gamePadState.Buttons.A == ButtonState.Pressed || state.IsKeyDown(Keys.E)) && canmove == true && canattack == true)
                {
                    foreach (var enemy in Game1.Enemies)
                        if (enemy.Value.GridPosition == new Vector2(whichWay().X, whichWay().Z) + gridspot)
                            enemy.Value.Alive = false;

                    foreach (Chest chest in Game1.chests)
                        if (chest.GridPosition == new Vector2(whichWay().X, whichWay().Z) + gridspot)
                            chest.Animate();
                    canmove = false;
                    canattack = false;
                }
                
                if (gamePadState.Triggers.Right == 0&& mouseState.LeftButton == ButtonState.Released)
                    canattack = true;





                if (canIGoThere(gridspot, direction) == true)
                {
                    DirectionToGo = new Vector3(direction.X, 0, direction.Y);
                    gridspot += direction/4;
                }
                  
                
            }
            if (canmove == false)
                foreach (var enemy in Game1.Enemies)
                    enemy.Value.speedresevoir += enemy.Value.speed;
        }
        public void smoothMoves(Vector3 place)
        {
            



            if (total > 0)
            {

                Position += (place / REALMOVESPEED);
                total--;

            }
            if (total <= 0)
            {
                donewithturn = true;
                total = REALMOVESPEED;
                DirectionToGo = Vector3.Zero;
            }
        }
        public bool canIGoThere(Vector2 whereIAm, Vector2 theDirection)
        {
            bool result = true;
            Vector2 testingVector = whereIAm + (theDirection / 4);
            if (Game1.Rooms.ContainsKey(testingVector))
            {
               
                foreach (var enemy in Game1.Enemies)
                    if (enemy.Value.GridPosition == testingVector)
                        result = false;
                
            }
            else
                result = false;
            return result;
        }


    }
    
}
