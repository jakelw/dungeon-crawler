﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace _3dGame
{
    class FormButton
    {
        Rectangle boundingBox = new Rectangle();

        public FormButton(Rectangle area)
        {
            boundingBox = area;
        }
        public bool mouseover()
        {
            MouseState state = Mouse.GetState();
            Rectangle mouserect = new Rectangle(state.X, state.Y, 1, 1);
            return mouserect.Intersects(boundingBox);
        }
    }
}
