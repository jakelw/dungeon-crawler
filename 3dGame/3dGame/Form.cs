﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace _3dGame
{
    class Form : Sprite
    {
        Dictionary<string,FormButton> buttons = new Dictionary<string,FormButton>();

        public void AddButton(string name,FormButton newButton)
        {
            buttons.Add(name, newButton);
        }
        public FormButton GetButton(string name)
        {
            if (buttons.ContainsKey(name))
                return buttons[name];
            else
                return new FormButton(new Rectangle());
        }
    }
}
