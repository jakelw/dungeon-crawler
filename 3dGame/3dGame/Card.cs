﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace _3dGame
{
    public class Card : Sprite
    {
        public uint Level = 1; // what level the card may be found on
        public int Cost = 0; // how much it costs to be in a deck
        public string name = "Empty";
        Sprite Image = new Sprite();
        Dictionary<Vector2,Sprite> symbols = new Dictionary<Vector2,Sprite>();
        public virtual void Spawn(ContentManager theContentManager, int DeckCost, string Name,string assetname)
        {
            this.LoadContent(theContentManager, "cardbacking");
            Position = new Vector2();
            Cost = DeckCost;
            name = Name;
            Image.LoadContent(theContentManager, assetname);
        }
        public void AddSymbol(Sprite symbol)
        {

            
            symbols.Add(new Vector2(-symbol.Size.Width+(symbol.Size.Width/2)-3,(-symbols.Count-1)*symbol.Size.Height)+new Vector2(this.Size.Width/2,this.Size.Height/2),symbol);
            
        }
       
        public override void Draw(SpriteBatch theSpriteBatch)
        {
            
            base.Draw(theSpriteBatch);
            Image.Scale = Scale;
            Image.Position = this.Position;
            Image.Draw(theSpriteBatch);
            foreach (var symbol in symbols)
            {
                symbol.Value.Scale = this.Scale;
                symbol.Value.Position = this.Position - symbol.Key*symbol.Value.Scale;
                symbol.Value.Draw(theSpriteBatch);
            }
            
        }
    }
}
