﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _3dGame
{
    public class Weapon:Card
    {
        public int Damage;
        public bool twoHanded = false;
        public bool Consumable = false;
    }
}
