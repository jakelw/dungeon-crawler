﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _3dGame
{
    public  class Item :Card
    {
        
        int boostStrength = 0;
        string slot = "none";
        int boostIntel=0;
        int boostSpeed=0;
        
        public void Spawn(Microsoft.Xna.Framework.Content.ContentManager theContentManager, int DeckCost, string Name)
        {
            name = "";
            base.Spawn(theContentManager, DeckCost, Name,"quantumGemeraldBlade");
            Sprite newsymbol = new Sprite();
            int X = Game1.rand.Next(0, 100);
            if (X >= 50)
            newsymbol.LoadContent(theContentManager, "attackSymbol");
            if (X <50)
                newsymbol.LoadContent(theContentManager, "magicSymbol");
            AddSymbol(newsymbol);
            name += newsymbol.AssetName;
            Sprite newersymbol = new Sprite();
            X = Game1.rand.Next(0, 100);
            if (X >= 50)
                newersymbol.LoadContent(theContentManager, "attackSymbol");
            if (X <50)
                newersymbol.LoadContent(theContentManager, "magicSymbol");
            AddSymbol(newersymbol);
             name += newersymbol.AssetName;

             Randomize(theContentManager, 1);
        }
        public void Spawn(Microsoft.Xna.Framework.Content.ContentManager theContentManager)
        {
        }
        public void Randomize(Microsoft.Xna.Framework.Content.ContentManager theContentManager,int level)
        {

            int type = Game1.rand.Next(0, 2);
            string typeString = "";
            if (type == 0)
            {
                typeString = "Usable";
                base.Spawn(theContentManager, 1, "Potion", "potion");
                Sprite newsymbol = new Sprite();
                newsymbol.LoadContent(theContentManager, "magicSymbol");
                AddSymbol(newsymbol);

            }
            if (type == 1)
            {
                typeString = "Weapon";
                base.Spawn(theContentManager, 1, "Sword", "quantumGemeraldBlade");
                Sprite newsymbol = new Sprite();
                newsymbol.LoadContent(theContentManager, "attackSymbol");
                AddSymbol(newsymbol);
            }
        }
        
    }
}
