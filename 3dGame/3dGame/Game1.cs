using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _3dGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        //important references
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public const int TRANSITIONSPEED = 12; //how fast do monsters move between squares
        public static SpriteFont font;
        public static Random rand = new Random(); //initialize the RNG
        public static Matrix ProjectionMatrix;
        public static Vector3 North = Vector3.Forward;
        public static Vector3 East = Vector3.Right;                 //not sure why I am using a middleman variable for these.
        public static Vector3 South = Vector3.Backward;
        public static Vector3 West = Vector3.Left;
        public static Player PlayerEntity = new Player(); // the player object
        static MouseState oldstate = Mouse.GetState(); //stores the mousestate from the last frame. Useful for checking changes between frames
        public static  Color bgColor = Color.Black;
        public static  Vector2 fog = new Vector2(1, 15); // distance from camera to fog
        public static Dictionary<Vector2, Thing> TheGrid = new Dictionary<Vector2, Thing>(); // where all the "thing"s are placed
        public static List<Card> allCards = new List<Card>(); // all the cards in the game
        public static Dictionary<Vector2, Enemy> Enemies = new Dictionary<Vector2, Enemy>(); //a dictionary storing the existing enemies
        public static Dictionary<Vector2, GridSpace> Rooms = new Dictionary<Vector2, GridSpace>();
        static bool cardDisplay = false; //am i in the cardDisplay screen?
        public static List<Thing> things = new List<Thing>(); // "thing"s are the base object of an entity in 3d space
        public static Dictionary<Vector2, Enemy> EnemiesToDelete = new Dictionary<Vector2, Enemy>(); //storage dictionary for holding the enemies to be deleted on the next frame
        public static List<Sprite> sprites = new List<Sprite>(); // all sprites in memory 
        DiggyGnome DiggingAI = new DiggyGnome(); // the AI that "digs out" the dungeon from solid blocks
        //end important references


        //Screen Identifiers (TODO: Make a switch)
        public static string ScreenToShow = "MainMenu";
        
        //end screen identifiers

        //TODO: Make Local
        public static bool clipping = true; //is collision detection on?
        public static string iamfacing = "none"; // this will store the direction i am facing.
        static Sprite bannerImage = new Sprite();
        public static Card selectedCard; //not sure why I'm using a global variable for this.
        public static List<Chest> chests = new List<Chest>();
        public static Card[] Hand = new Card[5]; //cards in the player's hand
        //end TODO

        public Game1()
        {
            
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            bannerImage.Position = new Vector2(400, 240);
            PlayerEntity.spawn(new Vector3(0,PlayerEntity.Height,0));
          // this.IsMouseVisible = true;
            int gridx = -25;
            int gridy = -25;
            Rooms = new Dictionary<Vector2, GridSpace>();

            int x = 0;
            while (x < 50)
            { //TEST: Spawn 50 random cards
                Item newCard = new Item();
                newCard.Spawn(this.Content, 0, "");
                newCard.Position = new Vector2(200 * x, 200);

                newCard.ChangeScale(.5f/(1-(Math.Abs(newCard.Position.X/400))));
                if (newCard.Position.X == 400)
                    newCard.ChangeScale(1);
                //newCard.Randomize(this.Content, 1);
                allCards.Add(newCard);
                x++;
            }

            while (gridy < 50)
            {
                while (gridx < 50)
                {
                    //fill the grid with filled in solid squares
                    GridSpace Square = new GridSpace();
                    Square.facing = rand.Next(1, 5);
                    Square.Spawn("filledSquare2", this.Content,new Vector2(gridx,gridy));
                    Rooms.Add(new Vector2(gridx,gridy),Square);
                    gridx++;
                   
                }
                gridx = -25;
                gridy++;
            }

            ProjectionMatrix = Matrix.CreatePerspectiveFieldOfView(.78f, graphics.PreferredBackBufferWidth /
                                         graphics.PreferredBackBufferHeight, .01f, 100);
            ProjectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45), 2, .01f, 200);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            font = Content.Load<SpriteFont>("SpriteFont1");
            bannerImage.LoadContent(this.Content, "ProjectDCBanner");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit

            
            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
              //  this.Exit();
            List<Vector2> newbies = new List<Vector2>();

            
            //TEST

            switch (ScreenToShow)
            {
                case "MainMenu":
                    //display main menu here
                    MouseState state = Mouse.GetState();
                this.IsMouseVisible = true;

                    FormButton newbutt = new FormButton(new Rectangle(290, 290, 300, 50)); //create the start game button
                    if (newbutt.mouseover() && state.LeftButton == ButtonState.Pressed && oldstate.LeftButton != ButtonState.Pressed)
                    {
                        ScreenToShow = "Dungeon";
                        oldstate = Mouse.GetState();
                        //enter the dungeon when clicked
                    }

                    FormButton newbutt2 = new FormButton(new Rectangle(262, 355, 300, 50)); //"Manage Deck Button"
                    if (newbutt2.mouseover() && state.LeftButton == ButtonState.Pressed && oldstate.LeftButton != ButtonState.Pressed)
                    {
                        ScreenToShow = "ManageCards";
                        bannerImage.LoadContent(this.Content, "ManageCards");
                        oldstate = Mouse.GetState();
                        //opens up the manage deck menu
                    }
                    break;
                    
                case "Dungeon":
                    this.IsMouseVisible = false; //remove mouse when in dungeon
                foreach (var Enemy in Enemies)
                {

                    Enemy.Value.update(); //enemies do their thing
                    if (Enemy.Value.Alive == false)
                        EnemiesToDelete.Add(Enemy.Key, Enemy.Value); //if an enemy is dead, move them to the removal dictionary
                }
                foreach (var enemy in EnemiesToDelete)
                    if (Enemies.ContainsValue(enemy.Value))
                        Enemies.Remove(enemy.Key); // remove enemies from main list if they are on the kill list
                foreach (var enemy in EnemiesToDelete)
                    if (things.Contains(enemy.Value))
                        things.Remove(enemy.Value);
                EnemiesToDelete = new Dictionary<Vector2, Enemy>();
                //ENDTEST

                DiggingAI.Begin(this.Content); // dig the dungeon
                GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);



                PlayerEntity.update();
                    break;

                case "ManageCards":
                     state = Mouse.GetState();
                this.IsMouseVisible = true;
                    //show the managecards screen
                    FormButton newbutton = new FormButton(new Rectangle(9, 300 , 170, 50));
                    if (newbutton.mouseover() && state.LeftButton == ButtonState.Pressed && oldstate.LeftButton != ButtonState.Pressed)
                    {//create "main menu" button
                       ScreenToShow = "MainMenu";
                        bannerImage.LoadContent(this.Content, "ProjectDCBanner");
                        oldstate = Mouse.GetState();
                    }
                    FormButton CardDisplay = new FormButton(new Rectangle(319, 277, 137, 188));
                    if (CardDisplay.mouseover() && state.LeftButton == ButtonState.Pressed && oldstate.LeftButton != ButtonState.Pressed)
                    {//display card button
                        ScreenToShow = "CardDisplay";
                        bannerImage.LoadContent(this.Content, "CardDisplay");
                        oldstate = Mouse.GetState();
                    }
                    break;
                case "CardDisplay":
                     float difference = 300;
                    KeyboardState keyState = Keyboard.GetState();
                    foreach (Card card in allCards)
                    {//show cards as larger when they are closer to the center of the screen
                        
                        if (Math.Abs(card.Position.X - 400) < difference)
                        {
                            selectedCard = card;
                            difference = Math.Abs(card.Position.X - 400);
                        }
                       
                        
                        if (keyState.IsKeyDown(Keys.D))
                            card.Position.X-=10;
                        if (keyState.IsKeyDown(Keys.A))
                            card.Position.X += 10;
                        card.ChangeScale( (float)Math.Pow((Math.Abs(card.Position.X-400)+1),-.25));
                        
                        if (card.Position.X == 400)
                            card.ChangeScale(1);
                    }
                    if (selectedCard != null)
                    {
                        //snap to closest card
                        if (selectedCard.Position.X > 400)
                            foreach (Card card in allCards)
                            card.Position.X-=5;
                        if (selectedCard.Position.X < 400)
                            foreach (Card card in allCards)
                            card.Position.X+=5;
                    }
                    break;
            }
            
                oldstate = Mouse.GetState();
            

            


            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(bgColor);

            foreach (Thing thing in things)
                thing.update();
            
            GraphicsDevice.DepthStencilState = new DepthStencilState() { DepthBufferEnable = true };

            // TODO: Add your drawing code here
            PlayerEntity.see(ProjectionMatrix);

            spriteBatch.Begin();

            if (ScreenToShow == "MainMenu") // if the main menu is still open
            {
                bannerImage.Draw(this.spriteBatch);
                if (ScreenToShow == "MainMenu")
                {//draw text on screen
                    spriteBatch.DrawString(font, "Start New Dungeon Crawlm", new Vector2(265, 300), Color.Black);
                    spriteBatch.DrawString(font, "Manage Decks", new Vector2(265, 350), Color.Black);
                }
                if (ScreenToShow == "CardDisplay")
                {
                    foreach (Card card in allCards)
                    {
                        if (card.Size.Intersects(new Rectangle(0,0,800,480)))
                        card.Draw(this.spriteBatch);
                    }
                    if (selectedCard != null)
                        spriteBatch.DrawString(font, selectedCard.name, new Vector2(400, 300), Color.Black);
                }
            }

            spriteBatch.DrawString(font, chests.Count.ToString(), new Vector2(100, 400), Color.Blue);
            int x = 0;
            //DEBUG: show debug data on screen when in dungeon
            foreach (Chest chest in chests)
            {x++;//DEBUG: Show location of chests
                spriteBatch.DrawString(font, chest.GridPosition.ToString(), new Vector2(100, 20*x), Color.Blue);
            }
            spriteBatch.DrawString(font, "Position" + PlayerEntity.gridspot.ToString(), new Vector2(150, 420), Color.Blue);
            //spriteBatch.DrawString(font, iamfacing, new Vector2(100, 440), Color.Blue);
            spriteBatch.DrawString(font, "" + PlayerEntity.rotationAxisY, new Vector2(100, 420), Color.Blue);
            spriteBatch.End();

            base.Draw(gameTime);
        }

    }
}
