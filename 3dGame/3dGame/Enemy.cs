﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _3dGame
{
    public class Enemy : Thing
    {
        public float speed = .33f;
         int REALMOVESPEED = Game1.TRANSITIONSPEED; // lower is faster
        public bool Alive = true;
        public Vector2 GridPosition = Vector2.Zero;
        public float speedresevoir = 0; 
        //Speed resevoir works like this: each time the player moves, a monster will add it's own speed to it's resevoir divided by the player's speed
        //This will result in monsters moving a number of times relative to the player's speed. a monster speed of 2 to a player speed of 1, for example,
        //will result in a monster moving 2 times for every movement a player makes.
        
        public Vector3 DirectionToGo = Vector3.Zero;
        public int total;

        

        public bool donewithturn = true;
        
        public void Spawn(string name, Vector2 location, ContentManager theContentManager,float stspeed)
    {
        speed = stspeed;
        REALMOVESPEED = (int)(Game1.TRANSITIONSPEED / speed) * (int)Game1.PlayerEntity.speed;
        if (speed < 1)
            REALMOVESPEED = Game1.TRANSITIONSPEED;
        total = REALMOVESPEED;

        int divider = REALMOVESPEED;
        GridPosition = location/4;
        base.Spawn(new Vector3(location.X, 0, location.Y), name, theContentManager);
            if (Game1.Enemies.ContainsKey(location) != true)
        Game1.Enemies.Add(location, this);
    }
  
        public void taketurn()
        {

            //if (donewithturn == false)

            

            if (speedresevoir >3)
                Alive = false;
            int randomdirection = Game1.rand.Next(1, 5);
            Vector3 direction = Vector3.Zero;
            if (randomdirection == 1)
                direction = Game1.North * 4;
            if (randomdirection == 2)
                direction = Game1.South * 4; 
            if (randomdirection == 3)
                direction = Game1.East * 4;
            if (randomdirection == 4)
                direction = Game1.West * 4;

            
                Vector2 distance = new Vector2(Math.Abs(GridPosition.X - Game1.PlayerEntity.gridspot.X), Math.Abs(GridPosition.Y - Game1.PlayerEntity.gridspot.Y));
                //direction = Vector3.Zero;
                DirectionToGo = Vector3.Zero;


                if (Vector2.Distance(GridPosition,Game1.PlayerEntity.gridspot) < 4)
                {
                    if (distance.X > distance.Y)
                    {
                        if (Game1.PlayerEntity.gridspot.X < GridPosition.X)
                        {
                            direction = new Vector3(-4, 0, 0);
                            // GridPosition += new Vector2(-1,0);
                            facing = 0;
                        }
                        if (Game1.PlayerEntity.gridspot.X > GridPosition.X)
                        {
                            direction = new Vector3(4, 0, 0);
                            // GridPosition += new Vector2(1,0);
                            facing = 0;
                        }
                    }
                    if (distance.X < distance.Y)
                    {
                        if (Game1.PlayerEntity.gridspot.Y < GridPosition.Y)
                        {
                            direction = new Vector3(0, 0, -4);
                            // GridPosition += new Vector2(0,-1);
                            facing = 1;
                        }
                        if (Game1.PlayerEntity.gridspot.Y > GridPosition.Y)
                        {
                            direction = new Vector3(0, 0, 4);
                            //GridPosition += new Vector2(0, 1);
                            facing = 1;
                        }
                    }
                }

                if (direction == Game1.North * 4)
                    facing = 1;
                if (direction == Game1.South * 4)
                    facing = 1;
                if (direction == Game1.East * 4)
                    facing = 0;
                if (direction == Game1.West * 4)
                    facing = 0;


                if (canIGoThere(GridPosition, new Vector2(direction.X, direction.Z)) == true)
                {
                    DirectionToGo = direction;
                    GridPosition += new Vector2(direction.X, direction.Z) / 4;
                    smoothMoves(direction);
                }
                
             
        }
        public void update()
        {

            REALMOVESPEED = (int)(Game1.TRANSITIONSPEED / speed)* (int)Game1.PlayerEntity.speed;
            if (speed < 1)
                REALMOVESPEED = Game1.TRANSITIONSPEED;

            if (REALMOVESPEED > 12)
                REALMOVESPEED = 12;


            if (donewithturn == false)
                smoothMoves(DirectionToGo);
            if (donewithturn == true && speedresevoir >= Game1.PlayerEntity.speed)
            {
                speedresevoir -= Game1.PlayerEntity.speed;
                taketurn();
            }

        }
        public void smoothMoves(Vector3 place)
        {
            donewithturn = false;

            
           
            if (total > 0)
            {
                
                Position += (place / REALMOVESPEED);
                Position.Y = .5f ;
                total--;
                

            }
            if (total <= 0)
            {
                Position.Y = 0;
                donewithturn = true;
                total = REALMOVESPEED;
           }
        }
        public bool canIGoThere(Vector2 whereIAm, Vector2 theDirection)
        {
            bool result = true;
            Vector2 testingVector = whereIAm + (theDirection/4);
            if (Game1.Rooms.ContainsKey(testingVector))
            {
                if (Game1.Rooms[testingVector].passable == false)
                    result = false;
                foreach (var enemy in Game1.Enemies)
                    if (enemy.Value.GridPosition == testingVector)
                        result = false;
                if (Game1.PlayerEntity.gridspot == testingVector)
                    result = false;
            }
            else
                result = false;
            return result;
        }
    }
}
